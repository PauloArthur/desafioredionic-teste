import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { MyApp } from './app.component';
import { ItemDetailsPage } from '../pages/item-details/item-details';
import { ListPage } from '../pages/list/list';
import { Pedidas } from '../pages/pedidas/pedidas';
import { MeuPerfil } from '../pages/meu-perfil/meu-perfil';
/*import { Contato } from '../pages/contato/contato';*/
import { TermosDeUso } from '../pages/termos-de-uso/termos-de-uso';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    Pedidas,
    ItemDetailsPage,
    ListPage,
    MeuPerfil,   
    /*ComoFunciona,*/
    TermosDeUso
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Pedidas,
    ItemDetailsPage,
    ListPage,
    MeuPerfil,
    /*ComoFunciona,*/
    TermosDeUso
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
