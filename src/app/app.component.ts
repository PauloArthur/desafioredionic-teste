import { Component, ViewChild } from '@angular/core';

import { Platform, MenuController, Nav } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Pedidas } from '../pages/pedidas/pedidas';
import { MeuPerfil } from '../pages/meu-perfil/meu-perfil';
//import { Contato } from '../pages/contato/contato';
import { TermosDeUso } from '../pages/termos-de-uso/termos-de-uso';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make Pedidas the root (or first) page
  rootPage: any = Pedidas;
  pages: Array<{title: string, component: any, icon: string}>;

  constructor(
    public platform: Platform,
    public menu: MenuController,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen
  ) {
    this.initializeApp();

    // set our app's pages
    this.pages = [
      { title: 'Pedidas', component: Pedidas, icon: 'beer' },
      { title: 'Meu Perfil', component: MeuPerfil, icon: 'person' },
      //{ title: 'Contato', component: Contato, icone: 'text'},
      { title: 'Termos de Uso', component: TermosDeUso, icon: 'list' }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }
}
