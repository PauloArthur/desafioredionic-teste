import { Component } from '@angular/core';

@Component({
	selector: 'perfil-page',
	templateUrl: 'meu-perfil.html'

})

export class MeuPerfil {
	perfis: Array<{nome: string, pedidasn: number, img: string}>;

	constructor(){
		this.initializePerfis();
	}

	initializePerfis(){
		this.perfis = [{nome: "Pedro Cicrano", pedidasn: 5, img: 'assets/img/perfil-vazio.jpg'},
		{nome: "Maria Fulano", pedidasn: 10, img: 'assets/img/perfil-vazio.jpg'},
		{nome: "João Beltrano", pedidasn: 0, img: 'assets/img/perfil-vazio.jpg'}
		]
	}
}