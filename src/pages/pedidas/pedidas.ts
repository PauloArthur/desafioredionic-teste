import { Component } from '@angular/core';

import { ItemDetailsPage } from '../item-details/item-details';

import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-pedidas',
  templateUrl: 'pedidas.html'
})

export class Pedidas {
  items: Array<{nome: string, preco: number, tempoh: number, img: string, descricao: string}>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.initializeItems();
  }
  
  initializeItems(){
  	this.items = [
  	
  	{nome: 'Carne', preco: 15, tempoh: 15, img:'assets/img/carne.jpg', descricao:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ut mi dui. Nullam vehicula in ipsum eget ornare. Suspendisse a sollicitudin erat, non sollicitudin ligula. Fusce sed feugiat ligula. Proin a lectus libero. Cras ultrices nibh erat, at eleifend arcu hendrerit eget. Maecenas non lorem ante. Vivamus sagittis egestas ipsum vitae venenatis."},
  	
  	{nome: 'Burger', preco: 10, tempoh: 23, img:'assets/img/burger.jpg', descricao: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ut mi dui. Nullam vehicula in ipsum eget ornare. Suspendisse a sollicitudin erat, non sollicitudin ligula. Fusce sed feugiat ligula. Proin a lectus libero. Cras ultrices nibh erat, at eleifend arcu hendrerit eget. Maecenas non lorem ante. Vivamus sagittis egestas ipsum vitae venenatis."},
  	
  	{nome: 'Cerveja', preco: 7, tempoh: 2, img: 'assets/img/beer.jpg', descricao: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ut mi dui. Nullam vehicula in ipsum eget ornare. Suspendisse a sollicitudin erat, non sollicitudin ligula. Fusce sed feugiat ligula. Proin a lectus libero. Cras ultrices nibh erat, at eleifend arcu hendrerit eget. Maecenas non lorem ante. Vivamus sagittis egestas ipsum vitae venenatis."}
  	];
  }

  getItems(ev){

  	this.initializeItems();

  	var val = ev.target.value;

        if (val && val.trim() != '') {
            /*return this.items.nome.filter(val);*/
    }
  }

  itemTapped(event, item) {
   
    this.navCtrl.push(ItemDetailsPage, {
      item: item
    });
  }

}
